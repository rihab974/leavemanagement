package com.example.gestion_conge.enumeration;

public enum Etat {
    SOLLICITE,
    VALIDE,
    REFUSE,
    ANNULE,
    EN_COURS,
    ARRETE,
    FINI

}
